/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.Bucket;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Line;
import com.jme3.scene.shape.Sphere;
import com.jme3.texture.Texture;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;
import static mygame.stoneColor.Black;
import static mygame.stoneColor.White;

/**
 *
 * @author stmirage
 */

/*
Шаблон имени геометрии для пустого поля cross_5_5_5 
 */
public class GameVisual {
    private Node gameField;
    private AssetManager assetManager;
    //private HashMap<Geometry,Boolean> registeredCrosses;
    private ArrayList<Node> registeredCrosses;
    private ArrayList<Node> registeredSpheres;
    private ArrayList<Node> registeredSpheresOV;
    private ArrayList<Geometry> registeredLastSpheresOv;
    private Stack<Geometry> lastMoveStack;
    public Material matWhite;
    public Material matBlack;
    public Material matRed;
    public Material matOrange;
    public Material overlayedMat;
    public Material LastMoveMaterial;
    public Material matBlackOV;
    public Material matWhiteOV;
    
    private int sOf;
    private int xDes;
    private int layers;
    
    
    public GameVisual(Node gameField, AssetManager assetManager,int sOf,int xDes){        
    this.registeredCrosses = new ArrayList<Node>();
    this.registeredSpheres = new ArrayList<Node>();
    this.registeredSpheresOV = new ArrayList<Node>();
    this.registeredLastSpheresOv = new ArrayList<Geometry>();
    this.gameField = gameField;
    this.assetManager = assetManager;
    
    setMaterials();
    

    lastMoveStack = new Stack<Geometry>();
    this.sOf = sOf;
    this.xDes = xDes;
    }
    
    public void createField(int sizeOfField){
        for(int i =-xDes;i<=xDes;i++){
            for(int j=-xDes;j<=xDes;j++){
                for(int l=-xDes;l<=xDes;l++){
                    gameField.attachChild(createCross("crossNode_"+(i+xDes)+(j+xDes)+(l+xDes), new Vector3f(i,j,l)));
                    gameField.attachChild(createSphereOV("sphereOV_"+(i+xDes)+(j+xDes)+(l+xDes), new Vector3f(i,j,l)));
                }
            }
        }
        this.layers = sizeOfField;
        
       DirectionalLight sun = new DirectionalLight();
       sun.setDirection(new Vector3f(1,0,-2).normalizeLocal());
       sun.setColor(ColorRGBA.White);
       gameField.addLight(sun);
       
       
       DirectionalLight sun2 = new DirectionalLight();
       sun2.setDirection(new Vector3f(-1,2,2).normalizeLocal());
       sun2.setColor(ColorRGBA.White);
       gameField.addLight(sun2);
        
    }
    
        private Node createCross(String name, Vector3f vector){
        Node crossNode = new Node(name);
        
        float x = vector.x;
        float y = vector.y;
        float z = vector.z;
        /*
        int LINE_WITDH = 1;
        float sz = 0.15f;
        crossNode.setLocalTranslation(vector);
        Line line1 = new Line(new Vector3f(x-sz, y, z), new Vector3f(x+sz, y, z));
        Line line2 = new Line(new Vector3f(x, y+ sz, z), new Vector3f(x, y -sz, z));
        Line line3 = new Line(new Vector3f(x, y, z+ sz), new Vector3f(x, y, z-sz));
        line1.setLineWidth(LINE_WITDH);
        line2.setLineWidth(LINE_WITDH);
        line3.setLineWidth(LINE_WITDH);
        
        Geometry g1 = new Geometry("cross_"+((int)x+4)+((int)y+4)+((int)z+4), line1);
        Geometry g2 = new Geometry("cross_"+((int)x+4)+((int)y+4)+((int)z+4), line2);
        Geometry g3 = new Geometry("cross_"+((int)x+4)+((int)y+4)+((int)z+4), line3);
        Material orange = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        ColorRGBA clr = (x==-4 && y==-4 && z==-4) ? ColorRGBA.Red : ColorRGBA.Orange;
        orange.setColor("Color", clr);
        g1.setMaterial(orange);
        g2.setMaterial(orange);
        g3.setMaterial(orange);
        crossNode.attachChild(g1);
        crossNode.attachChild(g2);
        crossNode.attachChild(g3);
        */
        
        crossNode.setLocalTranslation(vector);
        Sphere s = new Sphere(24,24,0.09f);
        Geometry geom = new Geometry("cross_"+((int)x+xDes)+((int)y+xDes)+((int)z+xDes), s);
        //Material orange = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        //ColorRGBA clr = (x==-xDes && y==-xDes && z==-xDes) ? ColorRGBA.Red : ColorRGBA.Orange;
        //orange.setColor("Color", clr);
        Material mt = (x==-xDes && y==-xDes && z==-xDes) ? matRed : matOrange;
        geom.setMaterial(mt);
        
        geom.setLocalTranslation(new Vector3f(x,y,z));
        crossNode.attachChild(geom);
        

        
        registeredCrosses.add(crossNode);
        return crossNode;
    }
        
    private Node createSphereOV(String name, Vector3f vector){
        Node sphereOVNode = new Node(name);
        float x = vector.x;
        float y = vector.y;
        float z = vector.z;
        
        sphereOVNode.setLocalTranslation(vector);
        Sphere overlaySphere = new Sphere(24,24,0.97f);
        Geometry overlayGeom = new Geometry("OVSphere_"+((int)x+xDes)+((int)y+xDes)+((int)z+xDes), overlaySphere);
        registeredLastSpheresOv.add(overlayGeom);
        overlayGeom.setQueueBucket(Bucket.Transparent); 
        overlayGeom.setMaterial(overlayedMat);
        overlayGeom.setLocalTranslation(new Vector3f(x,y,z));
        sphereOVNode.attachChild(overlayGeom);
        registeredSpheresOV.add(sphereOVNode);
        return sphereOVNode;
    }
        
    
        
    public void changeLayer(int x){
    layers +=x;
    if(layers>sOf) layers=sOf;
    if(layers<((sOf/2)+2)) layers=((sOf/2)+2);
    //System.out.println(layers);
    updateGeometry();
    }
    
    public void updateGeometry(){
        
        for (Node n : registeredCrosses){
            gameField.attachChild(n);
        }
        
        for (Node n : registeredSpheresOV){
            gameField.attachChild(n);
        }
        
        
        for(Node n : registeredCrosses){
            for(int l=sOf-1;l>=layers;l--){
                if(n.getName().contains(Integer.toString(l)) || n.getName().contains(Integer.toString(sOf-1-l))) {
                    gameField.detachChild(n);
                }
            }
        }
        
        for(Node n : registeredSpheresOV){
            for(int l=sOf-1;l>=layers;l--){
                if(n.getName().contains(Integer.toString(l)) || n.getName().contains(Integer.toString(sOf-1-l))) {
                    gameField.detachChild(n);
                }
            }
        }
        
        for(Node n : registeredSpheres){
            gameField.attachChild(n);
        }        
        
        for(Node n : registeredSpheres){
            for(int l=sOf-1;l>=layers;l--){
                if(n.getName().contains(Integer.toString(l)) || n.getName().contains(Integer.toString(sOf-1-l))) {
                    gameField.detachChild(n);
                }
            }
        }

    }
    
   public void addLastColor(String OVname){
        cleanOVColor();
        for (Geometry g : registeredLastSpheresOv ){
                if (g.getName().equals(OVname)){
                g.setMaterial(LastMoveMaterial);
                lastMoveStack.add(g);
            }
        }
    }
    public void cancelLastColor(){
        if (!lastMoveStack.isEmpty()) lastMoveStack.pop();
        cleanOVColor();
        if (!lastMoveStack.isEmpty()){
            Geometry g = lastMoveStack.pop();
            addLastColor(g.getName());}
    }
    
    private void cleanOVColor(){
        for (Geometry g : registeredLastSpheresOv){g.setMaterial(overlayedMat);}
    }
    
    public ArrayList<Node> getRegisteredCrosses(){
        return registeredCrosses;
    
    }
    
    public ArrayList<Node> getRegisteredSpheres(){
        return registeredSpheres;
    
    }
    
    public ArrayList<Node> getRegisteredSpheresOV(){
        return registeredSpheresOV;
    
    }
    
    
    
   public Node createSphere(String name, Vector3f vector, stoneColor color){
    
        float localx = vector.x;
        float localy = vector.y;
        float localz = vector.z;
        
        Node sphereNode = new Node(name);
        
        sphereNode.setLocalTranslation(localx,localy,localz);
        Sphere s = new Sphere(24,24,0.95f);
        Geometry geom = new Geometry("sphereg_"+((int)localx+xDes)+((int)localy+xDes)+((int)localz+xDes), s);
        
        geom.setLocalTranslation(new Vector3f(localx,localy,localz));
        
        switch(color){
            case Black:
                geom.setMaterial(matBlack);
                break;
            case White:
                geom.setMaterial(matWhite);
                break;
                    }
        sphereNode.attachChild(geom);
        registeredSpheres.add(sphereNode);
        return sphereNode;
    }
   
   public void destroySphere(String name){
       for (Iterator<Node> iter = registeredSpheres.listIterator(); iter.hasNext(); ) {
            Node n = iter.next();
            if (n.getName().equals(name)) {
                iter.remove();
                gameField.detachChildNamed(n.getName());
            }
        }
       
   }
   
   public void cleanSpheres(){
       /*
       for (Iterator<Node> iter = registeredSpheres.listIterator(); iter.hasNext(); ) {
            Node n = iter.next();
            //gameField.detachChildNamed(n.getName());
            n.removeFromParent();
            iter.remove();
            
        }
       */
       
       for (Node n : registeredSpheres){
           n.removeFromParent();
       }
       registeredSpheres = new ArrayList<Node>();
       
   }
   
   //метод сопоставляет присутствующую на поле геометрию и то,что есть в записях самого метода
   //после апдейтит поле как обычно
   public void refreshField(int[][][] field){
       cleanSpheres();
        for(int i =-xDes;i<=xDes;i++){
            for(int j=-xDes;j<=xDes;j++){
                for(int l=-xDes;l<=xDes;l++){
                    
                    stoneColor clr=stoneColor.None;
                    if(field[i+xDes][j+xDes][l+xDes]==1) clr=stoneColor.Black;
                    if(field[i+xDes][j+xDes][l+xDes]==2) clr=stoneColor.White;
                    if(clr!= stoneColor.None) {
                        Node n = createSphere("sphere_"+(i+xDes)+(j+xDes)+(l+xDes), new Vector3f(i,j,l),clr);
                        gameField.attachChild(n);
                        registeredSpheres.add(n);
                        
                    }
                }
            }
        }
    }
   
   public void destroyField(){
       gameField.detachAllChildren();
   }

   public final void setMaterials(){
    
    // Черный
    /*
    matBlack = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    Texture blackTexture = assetManager.loadTexture("Textures/black.jpg");
    matBlack.setTexture("ColorMap", blackTexture);
    */
    matBlack = new Material(assetManager,"Common/MatDefs/Light/Lighting.j3md");
    matBlack.setBoolean("UseMaterialColors",true);
    matBlack.setColor("Diffuse",ColorRGBA.Black);
    matBlack.setColor("Specular",ColorRGBA.Gray);
    matBlack.setFloat("Shininess", 64f);  // [0,128]   
    /*
    matWhite = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    Texture whiteTexture = assetManager.loadTexture("Textures/white.jpg");
    matWhite.setTexture("ColorMap", whiteTexture);
    */
    matWhite = new Material(assetManager,"Common/MatDefs/Light/Lighting.j3md");
    matWhite.setBoolean("UseMaterialColors",true);
    matWhite.setColor("Diffuse",ColorRGBA.White);
    matWhite.setColor("Specular",ColorRGBA.White);
    matWhite.setFloat("Shininess", 64f);  // [0,128]
    
    //Texture whiteTexture = assetManager.loadTexture("Textures/white.jpg");
    //matWhite.setTexture("ColorMap", whiteTexture);
    
    matRed = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    matRed.setColor("Color", ColorRGBA.Red);
    
    matOrange = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    //matOrange.setColor("Color", ColorRGBA.Orange);
    matOrange.setColor("Color", ColorRGBA.Gray);
    
    overlayedMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    ColorRGBA OverLayedClr = new ColorRGBA(0,0,0,0.0f);
    overlayedMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
    overlayedMat.setColor("Color", OverLayedClr);
    
    LastMoveMaterial = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    ColorRGBA LastMoveClr = new ColorRGBA(150,0,0,0.3f);
    LastMoveMaterial.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
    LastMoveMaterial.setColor("Color", LastMoveClr);
    
    matBlackOV = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    ColorRGBA BlackOVClr = new ColorRGBA(0,0,0,0.5f);
    matBlackOV.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
    matBlackOV.setColor("Color", BlackOVClr);
    
    matWhiteOV = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    ColorRGBA WhiteOVClr = new ColorRGBA(1.0f,1.0f,1.0f,0.5f);
    matWhiteOV.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
    matWhiteOV.setColor("Color", WhiteOVClr);
   }


}
