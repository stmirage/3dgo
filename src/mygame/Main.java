package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Sphere;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.system.AppSettings;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import com.jme3.input.controls.*;
import com.jme3.math.Vector2f;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.controls.ButtonClickedEvent;
import com.jme3.math.Ray;
import com.jme3.texture.Texture;
import de.lessvoid.nifty.builder.ElementBuilder;
import de.lessvoid.nifty.builder.TextBuilder;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.RadioButtonGroupStateChangedEvent;
import de.lessvoid.nifty.controls.ScrollPanel;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.ScreenController;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import static mygame.stoneColor.Black;
import static mygame.stoneColor.White;

/**
 * test
 * @author Nick Luzgarev
 */
public class Main extends SimpleApplication implements ScreenController {
    
    
    // Секция триггеров - назначение клавиш и мэппинга мыши. Тут полно лишнего, но структурно это никак не влияет на логику
    private final static Trigger LEFT_CLICK = new MouseButtonTrigger(MouseInput.BUTTON_LEFT);
    private final static Trigger RIGHT_CLICK = new MouseButtonTrigger(MouseInput.BUTTON_RIGHT);
    private final static Trigger MOV_LEFT = new MouseAxisTrigger(MouseInput.AXIS_X, true);
    private final static Trigger MOV_RIGHT = new MouseAxisTrigger(MouseInput.AXIS_X, false);
    private final static Trigger MOV_UP = new MouseAxisTrigger(MouseInput.AXIS_Y, true);
    private final static Trigger MOV_DOWN = new MouseAxisTrigger(MouseInput.AXIS_Y, false);
    private final static Trigger MOUSE_WHEEL_UP = new MouseAxisTrigger(MouseInput.AXIS_WHEEL,false);
    private final static Trigger MOUSE_WHEEL_DOWN = new MouseAxisTrigger(MouseInput.AXIS_WHEEL,true);
    private final static Trigger one = new KeyTrigger(KeyInput.KEY_1);
    private final static Trigger oneN = new KeyTrigger(KeyInput.KEY_NUMPAD1);
    private final static Trigger two = new KeyTrigger(KeyInput.KEY_2);
    private final static Trigger twoN = new KeyTrigger(KeyInput.KEY_NUMPAD2);
    private final static Trigger three = new KeyTrigger(KeyInput.KEY_3);
    private final static Trigger threeN = new KeyTrigger(KeyInput.KEY_NUMPAD3);
    private final static Trigger four = new KeyTrigger(KeyInput.KEY_4);
    private final static Trigger fourN = new KeyTrigger(KeyInput.KEY_NUMPAD4);
    private final static Trigger five = new KeyTrigger(KeyInput.KEY_5);
    private final static Trigger fiveN = new KeyTrigger(KeyInput.KEY_NUMPAD5);
    private final static Trigger six = new KeyTrigger(KeyInput.KEY_6);
    private final static Trigger sixN = new KeyTrigger(KeyInput.KEY_NUMPAD6);
    private final static Trigger seven = new KeyTrigger(KeyInput.KEY_7);
    private final static Trigger sevenN = new KeyTrigger(KeyInput.KEY_NUMPAD7);
    private final static Trigger eight = new KeyTrigger(KeyInput.KEY_8);
    private final static Trigger eightN = new KeyTrigger(KeyInput.KEY_NUMPAD8);
    private final static Trigger nine = new KeyTrigger(KeyInput.KEY_9);
    private final static Trigger nineN = new KeyTrigger(KeyInput.KEY_NUMPAD9);
    private final static Trigger enter = new KeyTrigger(KeyInput.KEY_RETURN);
    
    private final static Trigger eq_key = new KeyTrigger(KeyInput.KEY_EQUALS);
    private final static Trigger add_key = new KeyTrigger(KeyInput.KEY_ADD);
    
    private final static String MAPPING_ROTATE_LEFT = "RotateLeft";
    private final static String MAPPING_ROTATE_RIGHT = "RotateRight";
    private final static String MAPPING_ROTATE_UP = "RotateUp";
    private final static String MAPPING_ROTATE_DOWN = "RotateDown";
    private final static String MAPPING_CHANGE_FIELD_UP = "MouseWheelChangeLayersUP";
    private final static String MAPPING_CHANGE_FIELD_DOWN = "MouseWheelChangeLayersDOWN";
    
    private final static String MAPPING_1 = "1";
    private final static String MAPPING_2 = "2";
    private final static String MAPPING_3 = "3";
    private final static String MAPPING_4 = "4";
    private final static String MAPPING_5 = "5";
    private final static String MAPPING_6 = "6";
    private final static String MAPPING_7 = "7";
    private final static String MAPPING_8 = "8";
    private final static String MAPPING_9 = "9";
    private final static String MAPPING_ENTER = "enter";
    private final static String MAPPING_SET_STONE_BY_KEY = "setStoneByKey";
    //размер поля
    
    //int sOF = 5;
    //int xDes = 2;
    int sOF = 9;
    int xDes = sOF/2;
    int tmpsOF = 9;
    int tmpxDes = tmpsOF/2;
    
    // GameVisual - класс, отвечающий за прорисовку новых сфер и поля
    GameVisual gameVisual;
    // Суть этой переменной - показать в какие стороны от клетки можно двигаться
    // Используется в большинстве алгоритом свзяанных со съеданием камней и проч
    static  final int[][] local_movements = {{
        1,0,0},{
        -1,0,0},{
        0,1,0},{
        0,-1,0},{
        0,0,1},{
        0,0,-1},{
        0,0,0}
        };
        
    // InsideEmulator класс хранящий всю историю игры.Это нужно не для логирования
    // а для того, что по правилам игры нельзя делать ход, который приводит к повторению позиции на доске
    private InsideEmulator story = new InsideEmulator(sOF);
    // Логер.я пока не особо думал, каким он может быть, но пока он тупо сохраняет в файл ходы для партии
    // Это пожелание клиента
    private LoggerGameMovements gameLog = new LoggerGameMovements();
    // keyOrder - Клиент предпочел ,чтобы можно было ставить камни не только мышкой, но и
    // нажатием трех клавиш подряд и нажатием кнопки +
    // для того,чтобы правильно понимать какой сейчас номер клавиши и какой label заполнять на интерфейсе
    // ВВедена keyOrder, которая указывает для какого по счету нажатия какой label заполнять
    private static HashMap<Integer, String> keyOrder = new HashMap<Integer, String>();
    // две переменные, которые считают сколько уже пленников съела одна или другая сторона
    private static int blacks_eaten = 0;
    private static int blacks_eaten_tmp = 0;
    private static int whites_eaten = 0;
    private static int whites_eaten_tmp = 0;
    
    // Весь графический движок jmonkey использует для построения сцены иерархическую систему нод
    // fieldsNode - главная нода для поля игры 
    Node fieldsNode = new Node();
    // Массив field это логическое представление игры 0 - пустое место, 1 - черный камень 2 - белый 
    int[][][] field = new int[sOF][sOF][sOF];
    //переменные определяющие позицию и вертикальный угол камеры
    float xcam = 0;
    float ycam = 0;
    float zcam = 40;
    float yangle = 0;
    float zangle = 0;
    // Список материалов используемых в игре
    Material matWhite;
    Material matBlack;
    Material matRed;
    Material matOrange;
    Material matBlackOV;
    Material matWhiteOV;
    //clicked - вспомогательная переменная, хак, потому что у кликов в jmonkey есть особенности
    boolean clicked = false;
    // Цвет курсора
    ColorRGBA cursorColor;
    // Пока эти переменные для размера экрана не особо используются
    static int  WIDTH = 1280;
    static int HEIGHT = 720;
    // Массив, определяющие какие материалы используются в игре
    Material [] mats = {matWhite, matBlack};
    // само приложение jmonkey
    Main app;
    // Единственный пока в игре экран
    static NiftyJmeDisplay niftyDisplay;
    static Nifty nifty;
    // эта переменная обозначает текущий цвет для хода
    private stoneColor mvColor;
    int keyOrderIndex = 1;
    // Количество пассов в игре. Как только оба игрока спасуют - игра заканчивается
    int passes = 0;
    // Enum для определения состояния игры - игра продолжается или остановлена
    GameState gameState;
    
    // Enum для видимости поля
    MenuState menuState = MenuState.activeField;
    // в мейн у нас в основном инициализация самого приложения
    public static void main(String[] args) {
        keyOrder.put(1,"zField");
        keyOrder.put(2,"xField");
        keyOrder.put(3,"yField");
        
        AppSettings gameSettings = null;
        gameSettings = new AppSettings(false);
        gameSettings.setSettingsDialogImage("Textures/logo.png");
        gameSettings.setTitle("ShinBeit");
        //gameSettings.setIcons();
        Main app = new Main();


        app.setDisplayStatView(false);
        app.setDisplayFps(false);

        gameSettings.setResolution(WIDTH, HEIGHT);
        app.setSettings(gameSettings);
        app.start();
        
    }
    
    public Main() {

    }

    @Override
    public void simpleInitApp() {
    // ниты приложения. привязка меппинга управления
    
    flyCam.setMoveSpeed(50);
    flyCam.setEnabled(false);
    cam.setLocation(new Vector3f(xcam,ycam,zcam));
    cam.lookAt(new Vector3f(0,0,0), new Vector3f(0,0,0));
    //viewPort.setBackgroundColor(ColorRGBA.Gray);
    //viewPort.setBackgroundColor(ColorRGBA.White);
    ColorRGBA viewportColor = new ColorRGBA(0.85f,0.85f,0.85f,0.0f);
    viewPort.setBackgroundColor(viewportColor);
    // материал белого камня
    /*
    matWhite = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    Texture whiteTexture = assetManager.loadTexture("Textures/white.jpg");
    matWhite.setTexture("ColorMap", whiteTexture);
    */
    
    // Остальные цвета
    
    
 
    

    
    niftyDisplay = new NiftyJmeDisplay(assetManager, inputManager, audioRenderer, guiViewPort);
    nifty = niftyDisplay.getNifty();
    nifty.fromXml("Interface/GamePanel.xml", "start", this);
    nifty.registerScreenController(this);
    nifty.gotoScreen("start");
    guiViewPort.addProcessor(niftyDisplay);

    // Секция мэппинга
    inputManager.setCursorVisible(true);
    inputManager.addMapping(MAPPING_ROTATE_LEFT, MOV_LEFT, RIGHT_CLICK);
    inputManager.addMapping(MAPPING_ROTATE_RIGHT, MOV_RIGHT, RIGHT_CLICK);
    inputManager.addMapping(MAPPING_ROTATE_UP, MOV_UP);
    inputManager.addMapping(MAPPING_ROTATE_DOWN, MOV_DOWN);
    inputManager.addMapping("Clicked", RIGHT_CLICK);
    inputManager.addMapping("SetStone", LEFT_CLICK);
    inputManager.addMapping(MAPPING_CHANGE_FIELD_UP, MOUSE_WHEEL_UP);
    inputManager.addMapping(MAPPING_CHANGE_FIELD_DOWN, MOUSE_WHEEL_DOWN);
    inputManager.addMapping(MAPPING_1, one,oneN);
    inputManager.addMapping(MAPPING_2, two, twoN);
    inputManager.addMapping(MAPPING_3, three,threeN);
    inputManager.addMapping(MAPPING_4, four,fourN);
    inputManager.addMapping(MAPPING_5, five,fiveN);
    inputManager.addMapping(MAPPING_6, six,sixN);
    inputManager.addMapping(MAPPING_7, seven,sevenN);
    inputManager.addMapping(MAPPING_8, eight,eightN);
    inputManager.addMapping(MAPPING_9, nine,nineN);
    inputManager.addMapping(MAPPING_ENTER, enter);
    inputManager.addMapping(MAPPING_SET_STONE_BY_KEY, eq_key,add_key);
    
    
    inputManager.addListener(RRListener,new String[]{MAPPING_ROTATE_RIGHT, MAPPING_ROTATE_LEFT});
    inputManager.addListener(RLListener,new String[]{MAPPING_ROTATE_UP, MAPPING_ROTATE_DOWN});
    inputManager.addListener(CLICKED,new String[]{"Clicked"});
    inputManager.addListener(SETSTONE,"SetStone");
    inputManager.addListener(WheelListener, MAPPING_CHANGE_FIELD_UP,MAPPING_CHANGE_FIELD_DOWN);
    inputManager.addListener(MoveListener, MAPPING_1,MAPPING_2,MAPPING_3,MAPPING_4,MAPPING_5,MAPPING_6,MAPPING_7,MAPPING_8,MAPPING_9,MAPPING_ENTER);
    inputManager.addListener(SetKeyListener, MAPPING_SET_STONE_BY_KEY);
    
    // Создаем класс для графического отображеняи игры
    gameVisual = new GameVisual(fieldsNode,assetManager,sOF,xDes);
    gameVisual.createField(sOF);
        rootNode.attachChild(fieldsNode);
        fieldsNode.rotate(5.5f,0f,0f);
        fieldsNode.rotate(0,1.55f,0);
        fieldsNode.rotate(0,0,0.8f);
        mvColor = stoneColor.Black;
        cursorColor = ColorRGBA.Black;
        matWhite = gameVisual.matWhite;
        matBlack = gameVisual.matBlack;
        matOrange = gameVisual.matOrange;
        matRed = gameVisual.matRed;
        matWhiteOV = gameVisual.matWhiteOV;
        matBlackOV = gameVisual.matBlackOV;
            setGameState(GameState.inGame);
     /*Erectus(); */


    }
    
    
    
    
   // Вот это самый тупой лайфхак, но он нужен для нормального понимания зажатия клавиши
   private ActionListener CLICKED = new ActionListener() {

        public void onAction(String name, boolean isPressed, float tpf) {
            clicked = !clicked;
        
        }

    };
    // на левый клик мы ставим камень
     private ActionListener SETSTONE = new ActionListener() {

        public void onAction(String name, boolean isPressed, float tpf) {
            if(menuState == MenuState.activeField){
            if(gameState == GameState.inGame) setStone(mvColor);
            if(gameState == GameState.stoppedGame){ 
                //Screen screen = nifty.getCurrentScreen();
                //String x = screen.findNiftyControl("xField", Label.class).getText();
                //String y = screen.findNiftyControl("yField", Label.class).getText();
                //String z = screen.findNiftyControl("zField", Label.class).getText();
                //System.out.println(x+ ' '+ y+" "+z);
                //Geometry geom = getClosestGeom();
                /*try {
                    int xf = Integer.parseInt(x)-1;
                    int yf = Integer.parseInt(y)-1;
                    int zf = Integer.parseInt(z)-1;
                    List<int[]> l = new ArrayList<int[]>();
                    l.add(new int[]{xf,yf,zf});
                    List<int[]> newGroup = getGroup(l);
                    deleteGroup(newGroup);
                }
                catch(Exception e){}
                */
                
                Vector2f click2d = inputManager.getCursorPosition();
                Vector3f click3d = cam.getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 0f).clone();
                Vector3f dir = cam.getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 1f).subtractLocal(click3d);
                Ray ray = new Ray(click3d, dir);
                CollisionResults results = new CollisionResults();
                fieldsNode.collideWith(ray, results);
                if (results.size() > 0) {
                    CollisionResult closest = results.getClosestCollision(); 
                    Geometry geom = closest.getGeometry();
                    if (geom.getName().contains("OVSphere")){
                        int[] coords = coordHelper(geom.getName());
                        int xf = coords[0];
                        int yf = coords[1];
                        int zf = coords[2];
                        List<int[]> l = new ArrayList<int[]>();
                        l.add(new int[]{xf,yf,zf});
                        List<int[]> newGroup = getGroup(l);
                        if(newGroup.size()>0 && !(field[xf][yf][zf]==0)) deleteGroup(newGroup);
                    }
                }

            }
        }
        }

    }; 
    
    
    
    // Правой клавишей вертим поле
    private AnalogListener RLListener = new AnalogListener() {
        public void onAnalog(String name, float intensity, float tpf) {

           if (name.equals(MAPPING_ROTATE_UP) && clicked) {
           yangle += 1;
           zangle += 1;
           };
           if (name.equals(MAPPING_ROTATE_DOWN) && clicked) {
           yangle -= 1;
           zangle -= 1;           
           };
           
           if (yangle>89) yangle =89;
           if (zangle>89) zangle =89;
           if (yangle<-89) yangle =-89;
           if (zangle<-89) zangle =-89;
           
           
           cam.setLocation(new Vector3f(xcam, 
           (float) (ycam + Math.sin(Math.toRadians(yangle))*40), 
           (float) (zcam * Math.cos(Math.toRadians(yangle)))));
           cam.lookAt(Vector3f.ZERO, Vector3f.ZERO);
           

        }
    };
    
    private AnalogListener  RRListener = new AnalogListener() {
        public void onAnalog(String name, float intensity, float tpf) {
           
           if (name.equals(MAPPING_ROTATE_RIGHT) && clicked) fieldsNode.rotate(0,0.07f,0);
           if (name.equals(MAPPING_ROTATE_LEFT) && clicked) fieldsNode.rotate(0,-0.07f,0);
           
         
        }
    };
    
    // колесимком мы бираем или добавляем какие слои поля хотим видеть
    private AnalogListener WheelListener = new AnalogListener() {
        public void onAnalog(String name, float intensity, float tpf) {
            //System.out.println(name);
            //System.out.println(intensity);
            if (name.equals(MAPPING_CHANGE_FIELD_DOWN))gameVisual.changeLayer(-1);
            if (name.equals(MAPPING_CHANGE_FIELD_UP))gameVisual.changeLayer(1);
        }
    };
    // при наведении мы заполняем поля с цифрами тоем, на что релаьно наведен курсор
    private ActionListener MoveListener = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {
            if(keyPressed) {
            nifty
                    .getCurrentScreen()
                    .findNiftyControl(getKeyOrderIndexField(), Label.class)
                    .setText(name);
            };
        }
    };
    //  устанавливаем камень на клик
    private ActionListener SetKeyListener = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {
            setStone(mvColor);
        }
    };
    
    // simple - update - место которое производит действия каждую милисекунду времени
    @Override
    public void simpleUpdate(float tpf) {
        // вывод истории игры
       

        
        
        
        /*
        nifty.getCurrentScreen()
                .findNiftyControl("logLabel", Label.class)
                .setText(gameLog.getTextofGame());
                */
       //nifty.getCurrentScreen()
               
       //    .findNiftyControl("logLabel", TextField.class)
       //    .setText(gameLog.getTextofGame());
                //nifty.getCurrentScreen()
        //        .findNiftyControl("eatenLabel", Label.class)
        //        .setText("eaten\nBlacks: " + blacks_eaten + " Whites: " + whites_eaten+ "\n");
        Vector2f click2d = inputManager.getCursorPosition();
        Vector3f click3d = cam.getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 0f).clone();
        Vector3f dir = cam.getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 1f).subtractLocal(click3d);
        Ray ray = new Ray(click3d, dir);
        CollisionResults results = new CollisionResults();
        fieldsNode.collideWith(ray, results);
        if (results.size() > 0) {
          // The closest collision point is what was truly hit:
          CollisionResult closest = results.getClosestCollision();
          Geometry geom = closest.getGeometry();
          
          if (gameState == GameState.inGame){
              if (geom.getName().contains("OVSphere_")) {
              //System.out.println("switched");
              setControlTextSphere(geom.getName());
          }
              lightTheCross(geom.getName());
          }
          
          // переписать. теперь OV больше чем камень
          /*
          if (gameState == GameState.stoppedGame){
              if (geom.getName().contains("sphereg")) {
              setControlTextSphere(geom.getName());
 
          }   
              
              lightTheSphere(geom.getName());
          }
        */
        if (gameState == GameState.stoppedGame){
              if (geom.getName().contains("OVSphere_")) {
                //System.out.println(geom.getName());  
                ArrayList<String> spheres = new ArrayList<String>();
                List<int[]> l = new ArrayList<int[]>();
                int[] coords = coordHelper(geom.getName());
                int x = coords[0];
                int y = coords[1];
                int z = coords[2];
                l.add(coords);
                //System.out.println(x + " " + y + " "+z);
                if(field[x][y][z]!=0){
                    List<int[]> newGroup = getGroup(l);
                    for (int[] sp: newGroup){
                        int lx = sp[0];
                        int ly = sp[1];
                        int lz = sp[2];
                        //System.out.println("OVSphere_"+lx+ly+lz); 
                        spheres.add("OVSphere_"+lx+ly+lz);
                    }
                }
                //setControlTextSphere(geom.getName());
                lightTheSpheres(spheres);
 
          }   
              
              
          } 
        }
        
        
          
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    
    // getGeom method
    private Geometry getClosestGeom(){
        Vector2f click2d = inputManager.getCursorPosition();
        Vector3f click3d = cam.getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 0f).clone();
        Vector3f dir = cam.getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 1f).subtractLocal(click3d);
        Ray ray = new Ray(click3d, dir);
        CollisionResults results = new CollisionResults();
        fieldsNode.collideWith(ray, results);
        if (results.size() > 0) {
          CollisionResult closest = results.getClosestCollision();
          
          Geometry geom = closest.getGeometry();
          return geom;
        }
        return null;
    }
    // метод для выставления камня на поле
    private void putStone(int x, int y, int z, stoneColor stColor){
        passes = 0;
        keyOrderIndex=1;
        if (field[x][y][z] == 0 && (!story.isKo(x, y, z, stColor, field))) {
            fieldsNode.attachChild(gameVisual.createSphere("sphere_"+x+y+z, new Vector3f(x-xDes,y-xDes,z-xDes), stColor));
            gameVisual.addLastColor("OVSphere_"+((int)x)+((int)y)+((int)z));
            switch(stColor){
                case Black:
                    field[x][y][z] = 1;
                    break;
                case White:
                    field[x][y][z] = 2;
                    break;
            }
            
            stoneColor oppositeColor = (stColor == stoneColor.Black) ? stoneColor.White : stoneColor.Black;
            searchDeathAround(x,y,z,oppositeColor);
            List<int[]> l = new ArrayList<int[]>();
            l.add(new int[]{x,y,z});
            List<int[]> newGroup = getGroup(l);
            int grsize = newGroup.size();

            if (!hasDame(newGroup)) {deleteStoneWithoutCounting(x,y,z);}
            else {
            //добавить обработчик панели
            gameLog.addMove(new Move(x+1,y+1,z+1, stColor, action.added));
            addLabelToLogPanel();
            story.addTable(field);
            mvColor = (mvColor == stoneColor.Black) ? stoneColor.White : stoneColor.Black;
            cursorColor = (cursorColor == ColorRGBA.Black) ? ColorRGBA.White : ColorRGBA.Black;
            };
            
        }
        story.printStory();
        
    }
    

    // метод ищет, окружена ли группа и можно ли её съедать
    private void searchDeathAround(int x, int y, int z, stoneColor stColor){
       int clr = (stColor==stoneColor.Black) ? 1 : 2;
       for(int[] mov : local_movements){
       if (getStone(x+mov[0],y+mov[1],z+mov[2])==clr){
           List<int[]> tmpGroup = new ArrayList<int[]>();
           tmpGroup.add(new int[]{x+mov[0],y+mov[1],z+mov[2]});
           List<int[]> group = getGroup(tmpGroup);
           if (!hasDame(group)) {
               deleteGroup(group);
               story.addEatenEnemies(group.size());
               }
        }
       }
    }
    
    // удаление камня
    private void deleteStone(int x, int y, int z){
        int color = (field[x][y][z] == 1) ? 1 : 2;
        if (field[x][y][z] != 0) {
                  field[x][y][z] = 0;
                  gameVisual.destroySphere("sphere_"+x+y+z);
                  if(color == 1 && gameState==GameState.inGame) blacks_eaten+=1;
                  if(color == 2 && gameState==GameState.inGame) whites_eaten+=1;
                  if(color == 1 && gameState!=GameState.inGame) blacks_eaten_tmp+=1;
                  if(color == 2 && gameState!=GameState.inGame) whites_eaten_tmp+=1;
        }  
    }
    // иногда камень нужно удалить без игрового увеличения количества съеденны камней
    private void deleteStoneWithoutCounting(int x, int y, int z){
        int color = (field[x][y][z] == 1) ? 1 : 2;
        if (field[x][y][z] != 0) {
                  field[x][y][z] = 0;
                  gameVisual.destroySphere("sphere_"+x+y+z);
        }  
    }
    // удаление целой группы
    private void deleteGroup(List<int[]> ll){
        for(int[] l :ll){
            deleteStone(l[0],l[1],l[2]);
        }
        if(gameState==GameState.stoppedGame){
            story.addRedoTable(field);
        }
        

    }
    //подсчет очков
    private HashMap<stoneColor, Integer> getScores(){
    ArrayList<List<int[]>> groups = new ArrayList<List<int[]>>();
    HashMap<stoneColor, Integer> result = new HashMap<stoneColor, Integer>();
    ArrayList<Integer> container = new ArrayList<Integer>();
    int whites = 0;
    int blacks = 0;
    int seki = 0;
    
    for(int x=0;x<sOF;x++){
        for(int y=0;y<sOF;y++){
            for(int z=0;z<sOF;z++){
                int[] tmp = {x,y,z};
                if (field[x][y][z]==0 && !container.contains(formatContainerHelper(tmp))){
                    //System.out.println(x+" "+y+" "+z+" "+" - zero");
                    List<int[]> ll = new ArrayList<int[]>();
                    int[] arr = {x,y,z};
                    ll.add(arr);
                    List<int[]> lr = getGroup(ll);
                    for(int[] elem : lr){containerHelper(container,elem);}
                    if (!inGroups(groups,lr)){
                        groups.add(lr);
                    }
                }
            }
        }
    }
    
    for(List<int[]> group : groups){
        HashSet<stoneColor> set = getWhosTerritory(group);
        if((set.contains(stoneColor.Black) && set.contains(stoneColor.White)) ||
                (!set.contains(stoneColor.Black) && !set.contains(stoneColor.White))) 
            seki +=group.size();
        if(set.contains(stoneColor.Black) && !set.contains(stoneColor.White))
            blacks+=group.size();
        if(!set.contains(stoneColor.Black) && set.contains(stoneColor.White))
            whites+=group.size();

    }
    
    result.put(stoneColor.Black, blacks);
    result.put(stoneColor.White, whites);
    result.put(stoneColor.None, seki);
    return result;
    }
    // небольшой хелпер который перевеодит координаты во дно число
    private void containerHelper(ArrayList<Integer> list,int[] arr){
    list.add((arr[0]*100)+(arr[1]*10)+(arr[2]));
    }
    
    private void updateTmpSizeOfField(String tmpSize){
    tmpsOF = Integer.parseInt(tmpSize.substring(tmpSize.length() - 1));
    tmpxDes = tmpsOF/2;
    }
    
    private int formatContainerHelper(int[] arr){
        return (arr[0]*100)+(arr[1]*10)+(arr[2]);
    }
    // хелпер, переводящий строку с чиласми в сами числа
    private int[] coordHelper(String s){
        
        int[] result = {0,0,0};
        int x = (s.length()-3);
        int y = (s.length()-2);
        int z = (s.length()-1);
        result[0] = Integer.parseInt("" +s.charAt(x));
        result[1] = Integer.parseInt("" +s.charAt(y));
        result[2] = Integer.parseInt("" +s.charAt(z));
        
        
        return result;
    }
    // Проверка на то,что эти камни уже проверялись пи подсчете
    private boolean inGroups(ArrayList<List<int[]>> groups,List<int[]> group){
        ArrayList<int[]> bigList = new ArrayList<int[]>();
        for(List<int[]> list : groups){
            bigList.addAll(list);
        }
        for(int[] oneOfOld : bigList){
            for(int[] oneOfNew : group){
                if (oneOfOld.equals(oneOfNew)) return true;
            }
        }
        
        return false;
    }
    // Метод показывающий кому принадлжит захваченная территория
    private HashSet<stoneColor> getWhosTerritory(List<int[]> group){
        HashSet<stoneColor> result = new HashSet<stoneColor>();
        for(int[] stone: group){
             for(int[] mov : local_movements){
                int place = getStone(stone[0]+mov[0],stone[1]+mov[1],stone[2]+mov[2]);
                switch(place){
                case(1):
                    result.add(stoneColor.Black);
                    break;
                case(2):
                    result.add(stoneColor.White);
                    break;
                }
            }
        }
        if(result.isEmpty()) result.add(stoneColor.None);
        return result;
    }
    // Форматирование вывода
    private String formatScores(){
        String result = "";
        HashMap<stoneColor,Integer> m=getScores();
        for(Entry<stoneColor,Integer> e : m.entrySet()){
            result+= e.getKey()+" "+e.getValue()+"\n";
        }
        return result;
    
    }
    
    private String finalFormatScore(){
    
        String result = "Final scores: \n";
        HashMap<stoneColor,Integer> m=getScores();
        float blacksFinal = m.get(Black)+whites_eaten+whites_eaten_tmp;
        float whitesFinal = (float) m.get(White);
        whitesFinal+=0.5+blacks_eaten+blacks_eaten_tmp;
        result +="Black score: "+blacksFinal+"\n";
        result +="White score: "+whitesFinal+"\n";
        return result;
    
    }
    
    // метод позволяет получить группу, к которой принадлжеит камень - рекурсивен
    private List<int[]> getGroup(List<int[]> inGroup){
        if (inGroup.isEmpty()) return inGroup;
        List<int[]> group = inGroup;
        
        int[]l;l = group.get(0);
        int Xf = l[0];
        int Yf = l[1];
        int Zf = l[2];
        int color = field[Xf][Yf][Zf];
        int count = 0;

        ArrayList<int[]> tmpGroup;
        tmpGroup = new ArrayList<int[]>(group);
        for(int[] elem : tmpGroup){
             for(int[] mov : local_movements){
             if (getStone(elem[0]+mov[0],elem[1]+mov[1],elem[2]+mov[2])==color &&
                 !isInList(group, elem[0]+mov[0],elem[1]+mov[1],elem[2]+mov[2])
                ) {
                 int[] arr = {elem[0]+mov[0],elem[1]+mov[1],elem[2]+mov[2]};
                 group.add(arr);
                 count++;
             }
             }   
        }
        if (count>0) return getGroup(group);
        // если счетчик больше нуля - вернуть рекурсивно с новойгруппой
        // иначе вернуть группу
        return group;
    }
    // Получаем какой камень в конкретной точке
    private int getStone(int x, int y, int z){
        int stone;
        if (x>=0 && x<=(sOF-1) && y>=0 && y<=(sOF-1) && z>=0 && z<=(sOF-1) ) return field[x][y][z];
        return -1;
    }
    // Получаем есть или угрппы свободные клетки рядом или она мертва
    private boolean hasDame(List<int[]> group){
    
        for(int[] stone : group){
            for (int[] move : local_movements){
            int xl = stone[0] + move[0];
            int yl = stone[1] + move[1];
            int zl = stone[2] + move[2];
            if (getStone(xl,yl,zl)==0) return true;
            }
        }    
        return false;
};
    
    private boolean isInList(List<int[]> list,int x,int y, int z){
        for(int[] l : list){
            if (l[0]==x && l[1]==y && l[2]==z) return true;
        }
        return false;
    }
    // выставляем значения в интерфейсе
    private void setControlText(String in){
        Integer x = (Integer.parseInt(in.substring(6,7))+1);
        Integer y = Integer.parseInt(in.substring(7,8))+1;
        Integer z = Integer.parseInt(in.substring(8,9))+1;
        setLabels(x.toString(),y.toString(),z.toString());

    }
    // выставляем значения в интерфейсе
    private void setControlTextSphere(String in){
        int[] arr = coordHelper(in);
        Integer x = arr[0]+1;
        Integer y = arr[1]+1;
        Integer z = arr[2]+1;
        setLabels(x.toString(),y.toString(),z.toString());

    }
    // выставляем значения в интерфейсе
    private void setLabels(String x, String y, String z){
        Screen screen = nifty.getCurrentScreen();
        screen.findNiftyControl("xField", Label.class).setText(x);
        screen.findNiftyControl("yField", Label.class).setText(y);
        screen.findNiftyControl("zField", Label.class).setText(z);
    
    }
    // метод подсвечивающий крестик, на который наведен курсор
    // изменено на метод подсвечивающий всё сферу
    private void lightTheCross(String name){
        String crossName = OVSphereNameToCross(name);
        ArrayList<Node> gmCrosses = gameVisual.getRegisteredCrosses();
        ArrayList<Node> gm = gameVisual.getRegisteredSpheresOV();
        Material matNow = (mvColor== stoneColor.Black) ? matBlackOV : matWhiteOV;
        //Material matNowCross = (mvColor== stoneColor.Black) ? matBlack : matWhite;
        Material matNowCross =  matBlack ;
        for (Node n: gm){
        if (n.getChildren().get(0).getName().equals(name)) {n.setMaterial(matNow);}
        else {n.setMaterial(gameVisual.overlayedMat);}
        }
        for (Node n: gmCrosses){
        if (n.getChildren().get(0).getName().equals(crossName)) {n.setMaterial(matNowCross);}
        else {n.setMaterial(matOrange);}
        }
    }
// метод подсвечивающий сферу, которую надо удалить
   private void lightTheSphere(String name){
        ArrayList<Node> gm = gameVisual.getRegisteredSpheres();
        
        for (Node n: gm){
        if (n.getChildren().get(0).getName().equals(name)) {n.setMaterial(matRed);}
        else {
        int[] arr = coordHelper(n.getChildren().get(0).getName());
        Material matTmp = (getStone(arr[0],arr[1],arr[2]) == 1) ?
                      matBlack : matWhite;
        n.setMaterial(matTmp);
        }
        //else {n.getChildren().get(0).setMaterial(matOrange);}
        }
    }
   
  private void lightTheSpheres(ArrayList<String> spheres){
        ArrayList<Node> gm = gameVisual.getRegisteredSpheresOV();
        
        for (Node n: gm){
                //if (n.getChildren().get(0).getName().equals(name)) {n.setMaterial(matRed);}
                if (spheres.contains(n.getChildren().get(0).getName())) {n.setMaterial(matRed);}
                else n.setMaterial(gameVisual.overlayedMat);
        }

        //else {n.getChildren().get(0).setMaterial(matOrange);}

    }
    // тут объяснять надо)
   public String OVSphereNameToCross(String name){
   String localName = "cross"+name.substring(8);
   return localName;
   }
   
   
    
   //метод, ывставляющий камень, если это возможно по координатам введенным в поля
    public void setStone(stoneColor clr){
        //System.out.println("clr is " + clr);
         Screen screen = nifty.getCurrentScreen();
         String x = screen.findNiftyControl("xField", Label.class).getText();
         String y = screen.findNiftyControl("yField", Label.class).getText();
         String z = screen.findNiftyControl("zField", Label.class).getText();
         try {
         int xf = Integer.parseInt(x)-1;
         int yf = Integer.parseInt(y)-1;
         int zf = Integer.parseInt(z)-1;
         if (xf>-1 && xf<sOF && yf>-1 && yf<sOF && zf>-1 && zf<sOF){
             putStone(xf,yf,zf, clr);
             
         }
         
         }
         catch(Exception e){}
    }
    // сопоставление порядка клавиши для ввода в поле
    private String getKeyOrderIndexField(){
        String result = keyOrder.get(keyOrderIndex);
        keyOrderIndex++;
        if (keyOrderIndex>3) keyOrderIndex=1;
        return result;
    }
    
    private void setGameState(GameState gs){
        
        gameState = gs;
        switch(gameState){
            case inGame:
                nifty.getCurrentScreen().findElementByName("passButton").show();
                nifty.getCurrentScreen().findElementByName("CancelMove").show();
                nifty.getCurrentScreen().findElementByName("CancelDeleting").hide();
                nifty.getCurrentScreen().findElementByName("GiveUp").show();
                nifty.getCurrentScreen().findElementByName("ContiniueGame").hide();
                nifty.getCurrentScreen().findElementByName("FinalStop").hide();
                nifty.getCurrentScreen().findElementByName("showResult").hide();
                //nifty.getCurrentScreen().findElementByName("DelDeathLabel").hide();
                
                break;
            case stoppedGame:
                nifty.getCurrentScreen().findElementByName("passButton").hide();
                nifty.getCurrentScreen().findElementByName("CancelMove").hide();
                nifty.getCurrentScreen().findElementByName("CancelDeleting").show();
                nifty.getCurrentScreen().findElementByName("GiveUp").hide();
                nifty.getCurrentScreen().findElementByName("ContiniueGame").show();
                nifty.getCurrentScreen().findElementByName("FinalStop").show();
                nifty.getCurrentScreen().findElementByName("showResult").show();
                //nifty.getCurrentScreen().findElementByName("DelDeathLabel").show();
                break;
        }
    
    }
    
    
    private void addLabelToLogPanel(){
        String label = gameLog.getLastMove();
                
        
        TextBuilder textBuilder = new TextBuilder();
	textBuilder.text(label);
	textBuilder.width( 170 + "px" );
	textBuilder.height( 20 + "px" );
	textBuilder.font("Interface/Fonts/Default.fnt" );
	textBuilder.textHAlignLeft();
	textBuilder.build(nifty, nifty.getCurrentScreen(), 
                nifty.getCurrentScreen().findElementByName("log-panel"));
  
    }
    public void bind(Nifty nifty, Screen screen) {
        
    }

    public void onStartScreen() {
        
    }

    public void onEndScreen() {
        
    }
    // привязки к клавишам
 /*   
    @NiftyEventSubscriber(id="buttonBlack")
    public void onClickB(final String id, final ButtonClickedEvent event) {
        setStone(stoneColor.Black);
    }
    
    @NiftyEventSubscriber(id="buttonWhite")
    public void onClickW(final String id, final ButtonClickedEvent event) {
        setStone(stoneColor.White);
    }
*/    
    @NiftyEventSubscriber(id="buttonRemoveStone")
    public void onClickRS(final String id, final ButtonClickedEvent event) {
         Screen screen = nifty.getCurrentScreen();
         String x = screen.findNiftyControl("xField", Label.class).getText();
         String y = screen.findNiftyControl("yField", Label.class).getText();
         String z = screen.findNiftyControl("zField", Label.class).getText();
         try {
         int xf = Integer.parseInt(x)-1;
         int yf = Integer.parseInt(y)-1;
         int zf = Integer.parseInt(z)-1;
         if (xf>-1 && xf<sOF && yf>-1 && yf<sOF && zf>-1 && zf<sOF) {
            deleteStone(xf,yf,zf);}
       
         }
         catch(Exception e){}
    }    
    
        @NiftyEventSubscriber(id="buttonRemoveGroup")
    public void onClickRG(final String id, final ButtonClickedEvent event) {
        
        
         Screen screen = nifty.getCurrentScreen();
         String x = screen.findNiftyControl("xField", Label.class).getText();
         String y = screen.findNiftyControl("yField", Label.class).getText();
         String z = screen.findNiftyControl("zField", Label.class).getText();
         try {
         //System.out.println("DeleteEvent "+x+" "+y+" "+z);
         int xf = Integer.parseInt(x)-1;
         int yf = Integer.parseInt(y)-1;
         int zf = Integer.parseInt(z)-1;
         
         if (xf>-1 && xf<sOF && yf>-1 && yf<sOF && zf>-1 && zf<sOF) {
            
            List<int[]> ll = new ArrayList<int[]>();
            int[] arr = {xf,yf,zf};
            ll.add(arr);
   
            List<int[]> lr = getGroup(ll);
            deleteGroup(lr);
       
         }}
         catch(Exception e){}
    }

    @NiftyEventSubscriber(id="showResult")
    public void onShowResult(final String id, final ButtonClickedEvent event) {
    /*    int x = (int) (Math.random()*9);
        int y = (int) (Math.random()*9);
        int z = (int) (Math.random()*9);
        stoneColor color = stoneColor.values()[(int)(Math.random()*stoneColor.values().length)];
        //System.out.println("random event");
        //System.out.println(x+y+z+" "+color);
        putStone(x,y,z,color);
        story.printStory();
    }
    */
   nifty.getCurrentScreen()
   .findNiftyControl("eatenLabel", Label.class)
   .setText("Eaten:\nBlacks: " + (blacks_eaten + blacks_eaten_tmp) + " Whites: " + (whites_eaten + whites_eaten_tmp)+ "\n"+
           "Territory:\n" + formatScores()+"\n"+finalFormatScore());
      }
    
  @NiftyEventSubscriber(id="passButton")
    public void onClickPass(final String id, final ButtonClickedEvent event) {
      passes+=1;
      gameLog.addMove(new Move(0,0,0, mvColor, action.pass));
      addLabelToLogPanel();
      mvColor = (mvColor==stoneColor.Black) ? stoneColor.White : stoneColor.Black;
      if (passes>1) {
          nifty.getCurrentScreen()
            .findNiftyControl("eatenLabel", Label.class)
            .setText("Game Finished\nPLEASE, DELETE THE DEATHGROUPS"); 
          //gameState = GameState.stoppedGame;
          setGameState(GameState.stoppedGame);
        }
      }
  
  
    @NiftyEventSubscriber(id="new-game")
    public void onClickNewGame(final String id, final ButtonClickedEvent event) {
        gameVisual.cleanSpheres();
        gameVisual.destroyField();
        sOF = tmpsOF;
        xDes = tmpxDes;
        
        //int newSof = 
        gameVisual = new GameVisual(fieldsNode,assetManager,sOF,xDes);
        field = new int[sOF][sOF][sOF];
        blacks_eaten = 0;
        whites_eaten = 0;
        passes = 0;
        mvColor = stoneColor.Black;
        gameVisual.createField(sOF);
        story = new InsideEmulator(sOF);
        cursorColor = ColorRGBA.Black;
        //gameState = GameState.inGame;
        setGameState(GameState.inGame);
        gameLog = new LoggerGameMovements();
        nifty.getCurrentScreen().findElementByName("new-game-panel").hide();
        setLabels("", "", "");
        menuState = MenuState.activeField;
        nifty.getCurrentScreen()
            .findNiftyControl("eatenLabel", Label.class)
            .setText("");
      }
    
     @NiftyEventSubscriber(id="CancelMove")
     public void onClickCancelMove(final String id, final ButtonClickedEvent event) {
         if(gameState == GameState.inGame){
            
            mvColor = mvColor == stoneColor.Black ? stoneColor.White : stoneColor.Black;
            gameLog.addMove(new Move(0,0,0,mvColor,action.canceled));
            addLabelToLogPanel();
            if(passes<=0){
                field = story.getRedoMove();
                cancelEatingEnemies();
                gameVisual.refreshField(field);
                gameVisual.cancelLastColor();
                
                
            }
            else {
                passes-=1;
            }
         }
         //gameLog.removeLastMove();
      }
     @NiftyEventSubscriber(id="CancelDeleting")
     public void onClickCancelDeleting(final String id, final ButtonClickedEvent event) {
         if(gameState == GameState.stoppedGame && story.getRedoCounter()>0){
             field = story.getRedoMove();
             cancelEatingEnemies();
             gameVisual.refreshField(field);
             
         }
      }
     
     @NiftyEventSubscriber(id="FinalStop")
     public void onFinalStop(final String id, final ButtonClickedEvent event) {
         String str = "Eaten:\nBlacks: " + (blacks_eaten + blacks_eaten_tmp) + " Whites: " + (whites_eaten + whites_eaten_tmp)+ "\r\n"+
           "Territory:\r\n" + formatScores()+"\r\n"+finalFormatScore();
         nifty.getCurrentScreen().findElementByName("passButton").hide();
         nifty.getCurrentScreen().findElementByName("CancelMove").hide();
         nifty.getCurrentScreen().findElementByName("CancelDeleting").hide();
         nifty.getCurrentScreen().findElementByName("GiveUp").hide();
         nifty.getCurrentScreen().findElementByName("ContiniueGame").hide();
         nifty.getCurrentScreen().findElementByName("FinalStop").hide();
         nifty.getCurrentScreen().findElementByName("showResult").hide();
         nifty.getCurrentScreen()
            .findNiftyControl("eatenLabel", Label.class)
            .setText(str);
         gameLog.addStringToFile(str);
         menuState = MenuState.nonActiveField;
                //nifty.getCurrentScreen().findElementByName("DelDeathLabel").hide();
      }
     

     
     @NiftyEventSubscriber(id="GiveUp")
     public void onClickGiveUp(final String id, final ButtonClickedEvent event) {
     //gameState = GameState.stoppedGame;
     setGameState(GameState.stoppedGame);
     String str = "" + mvColor+" give up\r\n";
     nifty.getCurrentScreen()
     .findNiftyControl("eatenLabel", Label.class)
     .setText(str);
     gameLog.addStringToFile(str);
     nifty.getCurrentScreen().findElementByName("passButton").hide();
     nifty.getCurrentScreen().findElementByName("CancelMove").hide();
     nifty.getCurrentScreen().findElementByName("CancelDeleting").hide();
     nifty.getCurrentScreen().findElementByName("GiveUp").hide();
     nifty.getCurrentScreen().findElementByName("ContiniueGame").hide();
     nifty.getCurrentScreen().findElementByName("FinalStop").hide();
     nifty.getCurrentScreen().findElementByName("showResult").hide();
     menuState = MenuState.nonActiveField;
     
      }
     
     @NiftyEventSubscriber(id="ContiniueGame")
     public void onConGame(final String id, final ButtonClickedEvent event) {
     //gameState = GameState.inGame;
     setGameState(GameState.inGame);
     field = story.cancelDeleting();
     blacks_eaten_tmp = 0;
     whites_eaten_tmp = 0;
     gameVisual.refreshField(field);
     nifty.getCurrentScreen()
     .findNiftyControl("eatenLabel", Label.class)
     .setText("Continiue playing");
      }
     
     @NiftyEventSubscriber(id="game-menu")
     public void onGameMenu(final String id, final ButtonClickedEvent event) {
        if(menuState == MenuState.activeField) {
           nifty.getCurrentScreen()
           .findElementByName("new-game-panel").show();
           menuState = MenuState.nonActiveField;
        }
        else {
           nifty.getCurrentScreen()
            .findElementByName("new-game-panel").hide();
            menuState = MenuState.activeField;
        }
     }
     
    @NiftyEventSubscriber(id="fieldSize")
        public void onRadioGroup1Changed(final String id, final RadioButtonGroupStateChangedEvent event) {
        updateTmpSizeOfField(event.getSelectedId());
  }

    private void cancelEatingEnemies() {
        int enems = story.cancelEating();
        if (mvColor==stoneColor.Black) whites_eaten-=enems;
        else blacks_eaten -=enems;
    }
  
    
}
