/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

/**
 *
 * @author stmirage
 */
public class Move {
    private int x;
    private int y;
    private int z;
    private int enemiesEaten;
    
    private stoneColor color;
    private action actionOnMove;
    
    public Move(int x, int y, int z, stoneColor color,action actionOnMove){
    this.x = x;
    this.y = y;
    this.z = z;
    this.color = color;
    this.actionOnMove = actionOnMove;
    this.enemiesEaten = 0;
    }
    
    public int[] getCoords(){
    int[] result = {this.x,this.y,this.z};
    return result;
    }
    
    public stoneColor getColor(){
    return this.color;
    }
    
    public action getAction(){
    return this.actionOnMove;
            }
    
    public int getEnemiesEaten() {
        return this.enemiesEaten;
    }
    
    public void addEnemiesEaten(int i) {
        this.enemiesEaten+=i;
    }
    
    public String getStringRepresentation(){
        String result="";
        result+=this.color + " ";
        result+=this.actionOnMove+ " ";
        if (this.actionOnMove==action.added) result +=this.x + " " + this.y + " " + this.z;
        if (this.enemiesEaten > 0) result+= " eE" + this.enemiesEaten; 
        return result;
        
    }
}
