/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author stmirage
 */

/*
Логгер должен хранить
* Метаинфу по игре - Hashmap строка-строка
* Список ходов в хронологическом пордке - координаты камня, цвет камня, съеденная группа, в которой список камней
* Метаинфу по итогам игры - очки, территория - Hashmap строка-строка
 *
 *Возвращать как кажды список и хэшмэп, так и всё в текстовом виде 
 */
public class LoggerGameMovements {
    private HashMap<String,String> startInfo;
    private ArrayList<Move> moves;
    private HashMap<String,String> finishInfo;
    private String dir;
    private File myPath;
    private File logFile;
    private String fileName;

    public LoggerGameMovements() {
        this.startInfo = new HashMap<String,String>();
        this.finishInfo = new HashMap<String,String>();
        moves = new ArrayList<Move>();
        this.startInfo.put("Date", "Now");
        dir =new File(".").getAbsolutePath()+"/savegames";
        //dir = System.getProperty("user.dir")+"/savegames";
        //System.out.println(System.getProperty("user.dir"));
        myPath = new File(dir);
        myPath.mkdirs();
        DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy__HH-mm-ss");
        Date d = new Date();
        fileName = dateFormat.format(d);
        fileName = dir+"/"+fileName+".txt";
        //logFile = new File(dir+"/"+fileName+".txt");
        
    }
    
    public ArrayList<Move> getMoves(){
    return this.moves;
    }
    
    public String getTextofGame(){
    String result = "";
    for(Map.Entry<String, String> entry : this.startInfo.entrySet()){
    result += entry.getKey() + " " + entry.getValue()+ "\n";
    }
    int count = 1;
    for(Move s : moves){
        int[] coords = s.getCoords();
        String coordsTmp = (s.getAction()==action.pass) ? "" : "" + coords[2] + " " + coords[0] + " " + coords[1];
        result+= count + ") " + s.getColor() +" "+ coordsTmp +" "+ s.getAction() + "\n";
        count++;
    }
    return result;
    }
    
    public String getLastMove(){
        if(this.moves.size()>0){
            return moves.get(this.moves.size()-1).getStringRepresentation();
        };
        return "";
    }
    
    public void addEnemyEatensToLastMove(int i){
        if(this.moves.size()>0){
            moves.get(this.moves.size()-1).addEnemiesEaten(i);
        }; 
    }
    
    public void addMove(Move move){
        this.moves.add(move);
     try
        {   
            FileWriter writer = new FileWriter(fileName, true);
            
            String text = "" + this.moves.size() + ") " + move.getColor() + " " + move.getAction();
            text = text +" "+ ((move.getAction()==action.pass) ? "" : move.getCoords()[2]+" "+move.getCoords()[0]+" "+move.getCoords()[1]);
            writer.write(text);
            
            // запись по символам
            writer.append("\r\n");
         
            writer.flush();
        }
        catch(IOException ex){
            //System.out.println(ex.getMessage());
        } 
    }
    
    public void removeLastMove(){
        this.moves.remove(this.moves.size()-1);
     try
        {   
            FileWriter writer = new FileWriter(fileName, true);
            
            String text = "Last Move Cancelled"; 
            writer.write(text);
            
            // запись по символам
            writer.append("\r\n");
         
            writer.flush();
        }
        catch(IOException ex){
            //System.out.println(ex.getMessage());
        } 
    }
    
    public void addStringToFile(String str){
    
         try
        {   
            FileWriter writer = new FileWriter(fileName, true);
            writer.write(str);
            
            // запись по символам
            writer.append("\r\n");
         
            writer.flush();
        }
        catch(IOException ex){
            //System.out.println(ex.getMessage());
        } 
    }
    }

