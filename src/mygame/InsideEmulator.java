/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author stmirage
 */
public class InsideEmulator {
    private ArrayList<int[][][]> storyOfGame = new ArrayList<int[][][]>();
    private ArrayList<Integer> enemiesEatenStory = new ArrayList<Integer>();
    private int size;
    private int redoCounter;
    static  final int[][] local_movements = {{
        1,0,0},{
        -1,0,0},{
        0,1,0},{
        0,-1,0},{
        0,0,1},{
        0,0,-1},{
        0,0,0}
        };
    
    
    public InsideEmulator(int size){
        this.size = size;
        storyOfGame.add(new int[size][size][size]);
        enemiesEatenStory.add(0);
        redoCounter = 0;
        getEatensPodrobnee();
    }
    
    public int getRedoCounter(){
    System.out.println("Redo is " + redoCounter);
    return redoCounter;
    }
    
    public void addTable(int[][][] table){
        storyOfGame.add(cloneField(table));
        enemiesEatenStory.add(0);
        getEatensPodrobnee();
    }
    
    public void addEatenEnemies(int i){
        int j = enemiesEatenStory.get(enemiesEatenStory.size() - 1);
        j += i;
        enemiesEatenStory.set(enemiesEatenStory.size() - 1, j);
        getEatensPodrobnee();
    }
    
    public void addRedoTable(int[][][] table){
        storyOfGame.add(cloneField(table));
        enemiesEatenStory.add(0);
        redoCounter++;
    }
    
    public void printStory(){
    int count = 1;
    for(int [][][]elemTable : storyOfGame){
        //System.out.println("Move: "+ count);
        for(int i=0;i<size;i++){
            //System.out.println("x slice " + (i+1));
            for (int j=0;j<size;j++){
                for(int k=0;k<size;k++){
                    //System.out.print(elemTable[i][j][k]+" ");
                }
            //System.out.println();
            }
        }
        count++;
    }
    }
    
    public int[][][] cloneField(int[][][] arr){
        int[][][] result = new int[size][size][size];
        for(int i=0;i<size;i++){
            for (int j=0;j<size;j++){
                for(int k=0;k<size;k++){
                    result[i][j][k]=arr[i][j][k];
                }
            }
        }
        return result;
    }
    
    public boolean isEqual(int[][][] arr1, int[][][] arr2){
    
         for(int i=0;i<size;i++){
            for (int j=0;j<size;j++){
                for(int k=0;k<size;k++){
                    if (arr1[i][j][k] != arr2[i][j][k]) return false;
                }
            }
        }
        
    return true;
    }
    
    public boolean isKo(int x, int y, int z, stoneColor stColor, int[][][] field){
    boolean result = false;
    int[][][] tmpField = putStone(x,y,z,stColor,cloneField(field));
    
    for(int[][][] desk: storyOfGame){
        if(isEqual(desk,tmpField)) {
            System.out.println("this is KO!");
            result = true;}
    }
    
    return result;
    }
    
     public int[][][] getRedoMove(){
        //System.out.print(storyOfGame.size());
        if(storyOfGame.size()<=1) {
            //System.out.println("return empty field");
            return new int[size][size][size];}
        if(redoCounter>0) redoCounter--;
        storyOfGame.remove(storyOfGame.size()-1);
        //Вот тут разобраться какой индекс удалять
        
        return cloneField(storyOfGame.get(storyOfGame.size()-1));
    }
     
     
     
     public int[][][] cancelDeleting(){
         int[][][] result = storyOfGame.get(storyOfGame.size()-1);
         if(getRedoCounter()>0){ 
            while(getRedoCounter()>0){
                result = getRedoMove();
                cancelEating();
            }
         return result;
         }
         return result;
     }

    private boolean isInList(List<int[]> list,int x,int y, int z){
        for(int[] l : list){
            if (l[0]==x && l[1]==y && l[2]==z) return true;
        }
        return false;
    }
    
    private int getStone(int x, int y, int z, int[][][] field){
        if (x>=0 && x<=size-1 && y>=0 && y<=size-1 && z>=0 && z<=size-1 ) return field[x][y][z];
        return -1;
    }
    
    private boolean hasDame(List<int[]> group, int[][][] field){
        
      for(int[] stone : group){
           for (int[] move : local_movements){
                int xl = stone[0] + move[0];
                int yl = stone[1] + move[1];
                int zl = stone[2] + move[2];
                if (getStone(xl,yl,zl, field)==0) return true;
           }
        }    
    return false;
    };
    
    private List<int[]> getGroup(List<int[]> inGroup, int[][][]field){
        if (inGroup.isEmpty()) return inGroup;
        List<int[]> group = inGroup;
        
        int[]l;l = group.get(0);
        int Xf = l[0];
        int Yf = l[1];
        int Zf = l[2];
        int color = field[Xf][Yf][Zf];
        int count = 0;

        ArrayList<int[]> tmpGroup;
        tmpGroup = new ArrayList<int[]>(group);
        for(int[] elem : tmpGroup){
             for(int[] mov : local_movements){
             if (getStone(elem[0]+mov[0],elem[1]+mov[1],elem[2]+mov[2],field)==color &&
                 !isInList(group, elem[0]+mov[0],elem[1]+mov[1],elem[2]+mov[2])
                ) {
                 int[] arr = {elem[0]+mov[0],elem[1]+mov[1],elem[2]+mov[2]};
                 group.add(arr);
                 count++;
             }
             }   
        }
        if (count>0) return getGroup(group, field);
        return group;
    }
    
    private void deleteGroup(List<int[]> ll, int[][][] field){
        for(int[] l :ll){
            deleteStone(l[0],l[1],l[2],field);
        }
    }
    
    
    private void deleteStone(int x, int y, int z, int[][][] field){
        if (field[x][y][z] != 0) {
                  field[x][y][z] = 0;
        }  
    }
    
    private void searchDeathAround(int x, int y, int z, stoneColor stColor, int[][][] field){
       int clr = (stColor==stoneColor.Black) ? 1 : 2;
       for(int[] mov : local_movements){
       if (getStone(x+mov[0],y+mov[1],z+mov[2],field)==clr){
           List<int[]> tmpGroup = new ArrayList<int[]>();
           tmpGroup.add(new int[]{x+mov[0],y+mov[1],z+mov[2]});
           List<int[]> group = getGroup(tmpGroup, field);
           if (!hasDame(group, field)) deleteGroup(group, field);
        }
       }
    }
    
    private int[][][] putStone(int x, int y, int z, stoneColor stColor, int[][][] field){
        if (field[x][y][z] == 0) {   
            switch(stColor){
                case Black:
                    field[x][y][z] = 1;
                    break;
                case White:
                    field[x][y][z] = 2;
                    break;
            }
            
            stoneColor oppositeColor = (stColor == stoneColor.Black) ? stoneColor.White : stoneColor.Black;
            searchDeathAround(x,y,z,oppositeColor, field);
            List<int[]> l = new ArrayList<int[]>();
            l.add(new int[]{x,y,z});
            List<int[]> newGroup = getGroup(l,field);            
            int grsize = newGroup.size();
            
           //if(!hasDame(newGroup)) deleteGroup(newGroup);
            if (!hasDame(newGroup,field)) {deleteStone(x,y,z,field);}
           
        }
        return field;
    }
    
    public int cancelEating(){
        System.out.println("before canceling ");
        getEatensPodrobnee();
        if (enemiesEatenStory.size()>1) {
        int i = enemiesEatenStory.get(enemiesEatenStory.size()-2);
        enemiesEatenStory.remove(enemiesEatenStory.size()-2);
        
        System.out.println("after canceling ");
        getEatensPodrobnee();
        return i;
        }; 
        getEatensPodrobnee();
        return 0;
    };
    
    public void getEatensPodrobnee(){
        for (int s : enemiesEatenStory){
            System.out.print(s + " ");
        }
        System.out.println();
    }
}
